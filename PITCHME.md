# ТребаТак
#### Together we can!
_services for collective optimization of business processes_

[Screencast](https://4to.treba.ml/upload/2021/01/08/20210108213804-95090207.mp4)

---?include=topics/introduction/PITCHME.md
---?video=https://4to.treba.ml/upload/2020/12/25/20201225131842-52a65166.mp4&loop=true
---?include=topics/robochij_cikl_proektu/PITCHME.md
---?include=topics/nakopichennya_znan/PITCHME.md
---?include=topics/vityag_znan/PITCHME.md
---?include=topics/ctrukturuvannya_znan/PITCHME.md
---?include=topics/formalіzacіya_znan/PITCHME.md
---?include=topics/programna_realіzacіya/PITCHME.md
---?include=topics/korekcіya_formalіzovanih_znan/PITCHME.md
+++?include=/topics/end/PITCHME.md
