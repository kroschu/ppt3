
##### Author of the project [ТребаТак]()
![Volodymyr Kovalenko](assets/img/vokov.jpg)
<div class="align-points">
	<i class="fa fa-user"></i> Volodymyr Kovalenko > [resume](https://treba.ml/resume/)<br/>
	<i class="fa fa-github"></i> [github.com/kroschu](https://github.com/kroschu)<br/>
	<i class="fa fa-globe"></i> [Blog](https://treba.ml/blog/)<br/>
</div>


+++
> Every day, dedicated, thinking, sincere people contribute to making you do everything on their instructions. Such an obstacle is difficult to overcome. 
[Philip B. Crosby](https://en.m.wikipedia.org/wiki/Philip_B._Crosby)

+++

