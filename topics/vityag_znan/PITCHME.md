### Knowledge extraction
##### Knowledge extraction is the process of identifying data sources, knowledge, extracting it, describing it, and structuring it.

Undesirable effect (UDE). An UDE is a fact or an entity from reality that explains why it is so difficult to achieve the higher level of performance measurements.

+++

[![](https://4to.treba.ml/i.php?/upload/2021/01/08/20210108230032-3357a88f-xl.png)](https://rep-a.treba.ml/nbya-nomer-10.html) 
 Undesirable effect in the Wiki component, not structured

+++

#### [Current reality tree](https://rep-a.treba.ml/Derevo-potochno%D1%97-realnost%D1%96.html)

Current reality tree (CRT). One of the thinking processes in the theory of constraints, a current reality tree (CRT) is a way of analyzing many systems or organizational problems at once. By identifying root causes common to most or all of the problems, a CRT can greatly aid focused improvement of the system. A current reality tree is a directed graph.

+++

[![](https://4to.treba.ml/i.php?/upload/2021/01/08/20210108225250-f84f5606-xl.png)](https://km.treba.ml/cotonomas/1hpm778c19r4to3p)
[Дерево поточної реальності в базі знань](https://km.treba.ml/cotonomas/1hpm778c19r4to3p)
