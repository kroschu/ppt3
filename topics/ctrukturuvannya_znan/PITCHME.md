### Structuring knowledge
##### At this stage, the main concepts should be highlighted, and the structure of information presentation should be developed that has the maximum visibility, simplicity of changes and additions.
+++?video=https://4to.treba.ml/upload/2020/12/25/20201225132545-ea1e2160.mp4&loop=true


###  [Page in the project Wiki](https://rep-d.treba.ml/strukturuvannya-znan.html)
