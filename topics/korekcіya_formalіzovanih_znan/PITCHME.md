### Service
##### Correction of formalized data and knowledge - adding, updating, cleaning, that is, removing outdated information, filtering data and knowledge to find information that users need.

+++?video=https://4to.treba.ml/upload/2020/12/25/20201225120033-fe433e21.mp4&loop=true


###  [Page in the project Wiki](https://rep-d.treba.ml/korekc-ya-formal-zovanih-znan.html)

