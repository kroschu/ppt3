### Formalization of knowledge
##### Representation of structured information in machine processing formats. In languages describing data and knowledge, organization of automated processing and search for information on request.

+++?video=https://4to.treba.ml/upload/2020/12/25/20201225131513-ce06a2db.mp4&loop=true


###  [Page in the project Wiki](https://rep-d.treba.ml/formal-zac-ya-znan.html)
