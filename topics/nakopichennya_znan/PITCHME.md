### Accumulation of knowledge
##### Accumulation of information in the organization. For example, in the file system of the working environment of an existing information system, in email accounts, and so on.

+++?video=https://4to.treba.ml/upload/2020/08/10/20200810183839-17f763f3.mp4&loop=true

###  [Page in the project Wiki](https://doc.treba.ml/en/%D0%A2%D1%80%D0%B5%D0%B1%D0%B0%D0%A2%D0%B0%D0%BA/%D0%95%D1%82%D0%B0%D0%BF%D0%B8_%D1%80%D0%BE%D0%B7%D1%80%D0%BE%D0%B1%D0%BA%D0%B8_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D1%83/%D0%95%D1%82%D0%B0%D0%BF%D0%B8_%D1%80%D0%BE%D0%B7%D1%80%D0%BE%D0%B1%D0%BA%D0%B8_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D1%83%D0%9D%D0%B0%D0%BA%D0%BE%D0%BF%D0%B8%D1%87%D0%B5%D0%BD%D0%BD%D1%8F_%D0%B7%D0%BD%D0%B0%D0%BD%D1%8C)